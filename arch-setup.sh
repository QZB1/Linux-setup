#!/bin/bash

cd ~/   	# makes sure it's in home directory
pacman -S networkmanager 	# installs network manager
pacman -Syu
pacman -S base-devel	# gets git setup
pacman -S git
pacman -S --needed xorg	# installs gnome
pacman -S --needed gnome gnome-tweaks nautilus-sendto gnome-nettool gnome-usage gnome multi-writer adwaita-icon-theme chrome-gnome-shell xdg-user-dirs-gtk fwupd arc-gtk-theme seahosrse gdm
pacman -S --needed firefox vlc filezilla leafpad xscreensaver archlinux-wallpaper
systemctl enable gdm
systemctl enable NetworkManager
wget https://files.freezer.life/0:/PC/1.1.17/Freezer-1.1.17.AppImage # installs freezer
chmod +x Freezer-1.1.17.AppImage
pacman -S nitrogen # installs nitrogen file manager
pacman -S pcmanfm # installs pcmanfm file manager
mkdir Pictures # clones my pictures github repo
cd Pictures
git clone https://github.com/QuinnB07/Wallpapers.git
cd ~/
git clone https://aur.archlinux.org/snapd.git # installs snap
cd snapd
makepkg -si
sudo systemctl enable --now snapd.socket
sudo ln -s /var/lib/snapd/snap /snap
cd ~/
pacman -S neofetch # installs misc stuff
pacman -S cmatrix
pacman -S dosfstools
pacman -S vim
echo "neofetch" >> .bashrc # adds neofetch to the terminal
git clone https://gitlab.com/dwt1/st-distrotube.git # installs Distrotube's build of the suckless terminal
cd st-distrotube
make clean install
echo "Thanks for using the script!"
shutdown -r now
